# Server

[![npm](https://img.shields.io/npm/v/pubsweet-server.svg)](https://npmjs.com/package/pubsweet-server)
[![MIT license](https://img.shields.io/badge/license-MIT-e51879.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet/raw/master/LICENSE)
[![build status](https://gitlab.coko.foundation/pubsweet/pubsweet-server/badges/master/build.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-server/commits/master)

The `pubsweet-server` module handles connections to the [database layer](#database), contains the core [data models](#datamodels), and exposes a [REST API](#restapi) and [GraphQL API](#graphqlapi)

<h2 id="database">
  <a name="database"></a>
  Database layer
</h2>

`pubsweet-server` connects to a [PostgreSQL](https://www.postgresql.org/) database. 

### Ways of setting up PostgreSQL

There are a few ways to set up a PostgreSQL instace, ordered here by most recommended, descending: 
1. A `docker-compose` configuration is provided for easy creation of a PostgreSQL instance, which can be used with `docker-compose up db` or `yarn start:services` from an applications folder. In this case the apps are preconfigured and will connect to this database - no additional configuration is needed.
2. Local installation of the database with something like the [Postgres app (Mac)](https://postgresapp.com/), [PostgreSQL Windows installers](https://www.postgresql.org/download/windows/), or similar. You need to provide your own configuration details in `pubsweet-server.db` ([configuration details](https://node-postgres.com/features/connecting#programmatic)), for example: 

    ```js
    'pubsweet-server': {
      db: { 
        database: 'yourdatabasename',
        port: 5432,
        username: 'special',
        password: 'special2',
        host: 'localhost'
      }
    }
    ```
3. Remote/hosted PostgreSQL instance, for example Amazon's [RDS](https://aws.amazon.com/rds/). Same configuration options as above are needed here. This is mostly useful in a production setting, but can be used for development too, if more convenient.

<h2 id="datamodels">
  <a name="datamodels"></a>
  Data models
</h2>

The data models provided in `pubsweet-server` provide a foundation for most publishing applications:

- **knowledge** represented by [`Fragments`](#fragments) which can be organised in [`Collections`](#collections)
- **users** represented by [`Users`](#users) which can be organised in [`Teams`](#teams)

<!-- USERS -->

<h3>
  <span id="users">`User`</span> model <small>[[code on gitlab]](https://gitlab.coko.foundation/pubsweet/pubsweet/blob/master/packages/server/src/models/User.js)</small>
</h3>

#### example usage

```js
const User = require('pubsweet-server/src/models/User')

// create an admin user
const user = new User({
  username: 'admin',
  email: 'admin@website.net',
  password: 'correct-horse-battery-staple',
  admin: true
})

// save to the DB (this populates the object with its ID)
await user.save()

console.log('Saved admin user has ID:', user.id)
```

<!-- COLLECTIONS -->

<h3>
  <span id="collections">`Collection`</span> model <small>[[code on gitlab]](https://gitlab.coko.foundation/pubsweet/pubsweet/blob/master/packages/server/src/models/Collection.js)</small>
</h3>

A `Collection` is a container for [`Fragment`s](#fragments). It is an abstraction to represent any high-level grouping of published units like a book, a journal, an issue, a blog, and so on. Every PubSweet app must have at least one collection.

#### example usage

```js
const Collection = require('pubsweet-server/src/models/Collection')

const title = 'journalOfKnowledge'
const created = Date.now()

// create a new collection
const collection = new Collection({ title, created })

// set the owner of the collection
// requires that we have a previously saved User instance
collection.setOwners([user.id])

// save to the DB (this populates the object with its ID)
await collection.save()

console.log('Created collection with ID: ', collection.id)
```

<!-- FRAGMENTS -->

<h3>
  <span id="fragments">`Fragment`</span> model <small>[[code on gitlab]](https://gitlab.coko.foundation/pubsweet/pubsweet/blob/master/packages/server/src/models/Fragment.js)</small>
</h3>

A `Fragment` represents a published unit like a chapter, an article, a blog post, a comment, and so on. `Fragment`s can belong to one or more [`Collection`s](#collections). It can be owned by one or more [`User`s](#users).

### example usage

```js
const Collection = require('pubsweet-server/src/models/Collection')
const journal = await collection.findByField('name', 'journalOfKnowledge')

const Fragment = require('pubsweet-server/src/models/Fragment')

const opts = {
  title: 'A proof that P=NP'
}
const article = new Fragment(opts)

journal.addFragment(article)

article.setOwners([req.user])

console.log(`New team ${article.name} created with ID: ${article.id}`)
```

<!-- TEAMS -->

<h3>
  <span id="teams">`Team`</span> model <small>[[code on gitlab]](https://gitlab.coko.foundation/pubsweet/pubsweet/blob/master/packages/server/src/models/Team.js)</small>
</h3>

### example usage

```js
// teams are created around collections or fragments
const Collection = require('pubsweet-server/src/models/Collection')
const journal = await findByField('name', 'journalOfKnowledge')

const Team = require('pubsweet-server/src/models/Team')

const opts = {
  name: 'Journal of Knowledge Editors',
  teamType: {
    name: 'Production Editor',
    permissions: 'all'
  },
  object: journal
}
const editors = await new Team(opts)

console.log(`New team ${editors.name} created with ID: ${editors.id}`)
```

<!-- AUTHSOME -->

<h2 id="authsome">
  `Authsome` <small>[[code on gitlab]](https://gitlab.coko.foundation/pubsweet/authsome)</small>
</h2>

Is a module that implements ABAC (attribute-based access control), to determine whether users are authorized to perform requested operations, based on:

- the user
- the type of operation
- the target resource of the operation

### example usage

`Authsome` can be used in isolation, like this:

```js
const Authsome = require('authsome')
const authsome = new Authsome({...config.authsome, mode})

const user = 'user1'
const operation = 'POST'
const resource = '/fragments/12345'

const haspermission = authsome.can(user, operation, resource) // is true or false
```

However, it is usually used inside an express route function, checking the authorization data provided in the HTTP request to see if the endpoint operation is permitted.

Here's an example of how a route might use `authsome`:

```js

api.delete('/users/:id', authBearer, async (req, res, next) => {
  try {
    let user = await User.find(req.params.id)
    const permission = await authsome.can(req.user, req.method, user)

    if (!permission) {
      throw authorizationError(req.user, req.method, req.path)
    }
    user = await user.delete()
    return res.status(STATUS.OK).json(user)
  } catch (err) {
    next(err)
  }
})


```

You can see real examples of Authorize in use wherever `pubsweet-server` defines an API endpoint, for example [the `GET /teams` endpoint](https://gitlab.coko.foundation/pubsweet/pubsweet/blob/master/packages/server/src/routes/api_teams.js#L44-48), which calls `Authorize` in this [applyPermissionFilter helper](https://gitlab.coko.foundation/pubsweet/pubsweet/blob/master/packages/server/src/routes/util.js#L114-127).

<h2 id="restapi">
  <a name="restapi"></a>
  REST API
</h2>

`pubsweet-server` ships with a REST API that includes the following endpoints:

<br>

- `/api/collections/:id/teams`
  <span class="api-methods">`GET` `POST`</span>
- `/api/collections/:id/teams/:id`
  <span class="api-methods">`GET` `DELETE` `PUT`</span>
- `/api/collections`
  <span class="api-methods">`POST` `GET`</span>
- `/api/collections/:id`
  <span class="api-methods">`GET` `PATCH` `DELETE`</span>
- `/api/collections/:id/fragments`
  <span class="api-methods">`POST` `GET`</span>
- `/api/collections/:collectionId/fragments/:fragmentId`
  <span class="api-methods">`GET` `PATCH` `DELETE`</span>
- `/api/fragments`
  <span class="api-methods">`POST`</span>
- `/api/fragments/:fragmentId`
  <span class="api-methods">`GET` `PATCH` `DELETE`</span>
- `/api/upload`
  <span class="api-methods">`POST`</span>
- `/api/users/authenticate`
  <span class="api-methods">`POST` `GET`</span>
- `/api/users`
  <span class="api-methods">`POST` `GET`</span>
- `/api/users/:id`
  <span class="api-methods">`GET` `DELETE` `PUT`</span>
- `/api/teams`
  <span class="api-methods">`GET` `POST`</span>
- `/api/teams/:id`
  <span class="api-methods">`GET` `DELETE` `PUT`</span>
- `/updates`
  <span class="api-methods">`GET`</span>

<h2 id="graphqlapi">
  <a name="graphqlapi"></a>
  GraphQL
</h2>

If configured (if `pubsweet-server.enableExpirementalGraphql` is set to `true`) a GraphQL API is available at `/graphql`. To explore the features of the API you can use GraphiQL, which (if configured i.e. if `pubsweet-server.graphiql` is set to `true`) is hosted at `/graphiql`. 

