# 1. First component

We'll start by making an extremely simple PubSweet component which renders a static element using [React](https://facebook.github.io/react/). The component will display "Hello world!"

### 1. Navigate to `[your_app_name]/app/components/`
### 2. Add a new file called `pubsweet-component-hello-world.jsx`
### 3. Create the PubSweet component in `pubsweet-component-hello-world.jsx`:

```jsx
import React from 'react'

export default class Hello extends React.Component {
  render() {
    return (
      <div>Hello world!</div>
    )
  }
}
```

This component does 3 things:

1. Imports the React javascript library so we can use it:
```jsx
import React from 'react'
```

2. Creates a component that renders `<div>Hello world!</div>` to a page:

  ```jsx
  class Hello extends React.Component {  
    render() {
      return (  
        <div>Hello world!</div>  
      )  
    }
  }
  ```

3. Exports the component so it can be used in other files: `export default`

Now that we have a component, let's [serve it up using PubSweet](./2_route.html).
