# 7. Load the component

We've created an action and a reducer for our component. Next, we are going to update our components `index.js` to export the action and reducer, then we'll `add` it to PubSweet.

### 1. Update `index.js` to export the action and reducer

Add the action and reducer to the object exported by `index.js`:
```js
module.exports = {
  client: {
    components: [
      () => require('./pubsweet-component-hello-world')
    ],
    actions: () => require('./actions'),
    reducers: () => require('./reducers')
  }
}
```

Exporting this object is important for the next step.

### 2. `add` the component

From the root directory of your app, run `pubsweet add [path-to-component]`.

`pubsweet add [component name]` normally looks for a component on npm. But, since we've developed a component locally, we'll give it the absolute path to the directory that holds our component.  It will look something like this:
```jsx
pubsweet add /Users/john/johns_pubsweet_app/app/components/pubsweet-component-hello-world
```
If it's successful, you'll see something like this:
Look for these messages:
```bash
success Saved 1 new dependency.
└─ pubsweet-component-hello-world@0.1.0
```
and that the component is being added to the dependencies:
```bash
info: Reading dependencies
info: Adding 1 components to config
info: Components being added: pubsweet-component-hello-world
info: Adding components to config
info: Finished updating config
info: All 1 components installed
```
If you see an `error Couldn't find package`, check that the path you're providing points to the folder that holds your package.

### 3. Verify that the component has been added to the `config/components.json` file.

Open up the `config/components.json` with a text editor. If everything went well, you should see that "pubsweet-component-hello_world" has been added to the end of the array of components:
```json
["pubsweet-component-blog",
 "pubsweet-component-login",
 "pubsweet-component-manage",
 "pubsweet-component-pepper-theme",
 "pubsweet-component-posts-manager",
 "pubsweet-component-signup",
 "pubsweet-component-hello-world"]
```

### 4. `npm install` to reflect the changes
Make sure you run `npm install` from the root directory of your app. If you don't `npm install` after `add`ing, you'll get a webpack error when you try to `pubsweet start` the app.

### 5. Run the app

After `npm install` is complete, run `pubsweet start` to start the server.  As it starts up, look for a line in the terminal that indicates the `pubsweet-app-hello-world` component has been loaded:
```bash
info: Registered component pubsweet-component-hello-world
```

At this point, PubSweet should have registered our reducer, and everything should be working. Try it out! Running PubSweet in dev mode, pull up your component and click your button. In dev mode, PubSweet will log the `prev state`, the dispatched action, and the `next state`. If you inspect the `prev state`, you should see a key-value `clickStatus: "unclicked"`. Inspect the `next state` object, and you should see that `clickStatus` now reads "clicked".
