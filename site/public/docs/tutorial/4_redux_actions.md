# 4. Give the component Redux building blocks

Right now, our component isn't very exciting: it just renders static content. Let's change that by giving it some of the basic building blocks it needs to interact with the rest of the PubSweet app.

PubSweet Client is a [Redux app](http://redux.js.org/):
* Its entire client state is stored in a single javascript object (the `store`)
* To update the store, a component must `dispatch` an action object that contains:
  1. the `type` of the action
  2. any other required data (e.g. new data to add to the store)
* Each time an action is dispatched, the store executes a list of functions (`reducers`) that take in the previous state and the dispatched action object, then return the new state.

You can read more about these fundamental Redux concepts [here](http://redux.js.org/docs/introduction/ThreePrinciples.html).

For our component to be able to interact with the rest of the PubSweet Client state, it needs to know how to dispatch actions, and it needs to export a `reducer` the store can use to respond to actions. Let's start with actions.

## Add a button

Let's change our component to render a button that dispatches an action, which in turn updates the PubSweet store.

First, render the button and define a function `helloClick` to run when the button is clicked. Update `pubsweet-component-hello-world.jsx` to the following:
```jsx
import React from 'react'

export default class Hello extends React.Component{
  helloClick() {
    console.log('helloClick')
  }

  render() {
    return (
      <div>
        <button type="button" onClick={this.helloClick}>click me</button>
      </div>
    )
  }
}
```
The button runs `helloClick` when it's pressed. Run PubSweet in dev mode and verify that pressing the button logs "helloClick" to the console when you press it.

## Add an action
Next, when we click the button, we want it to `dispatch` an action object that looks like this:
`{ type: 'BUTTONCLICK' }`

All our action does is identify itself with its `type`. We'll define the `type` strings as constants in their own file, which we'll import when we want to make actions.

Add these two files to the component's directory:

1. `types.js`:

  ```jsx
  export const BUTTONCLICK = 'BUTTONCLICK'
  ```
2. `actions.js`:

  ```jsx
  import * as T from './types'

  export function helloClick() {
    return { type: T.BUTTONCLICK }
  }
  ```

`actions.js` defines a function we'll use to create action objects to `dispatch`, and that function uses the `types.js` constants to make the actions.

Let's import this `helloClick` function we've just built into `pubsweet-component-hello-world.jsx`, delete the component's old `helloClick` function, and update the button's `onClick` function (we'll also update how we `export` very slightly):

```jsx
import React from 'react'
import { helloClick } from './actions'

class Hello extends React.Component{
  render() {
    return (
      <div>
        <button type="button" onClick={helloClick}>click me</button>
      </div>
    )
  }
}

export default Hello
```

Now when we click the button, `helloClick` returns our action object with the single key-value pair `{ type: 'BUTTONCLICK' }`. You can test it by adding a `console.log('worked')` to the `helloClick` function in `action.js` and checking that the browser logs "worked" when you click the button:
```jsx
export function helloClick() {
  console.log('it works')
  return {
    type: T.BUTTONCLICK
  }
}
```
Delete the `console.log` line once you've tested it.


_Really_ what we want to do is dispatch this action object. The `dispatch` method comes from the store - so let's [hook into the store](./5_connect.html).
