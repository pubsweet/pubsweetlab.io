# 3. Package your component

Now that you've got a working component, we're going to make it into a package that can be installed by PubSweet as a frontend component.

### 1. Make a new folder for the component

1. In the `/app/components` directory, add a new folder called `pubsweet-component-hello-world`
2. Move the `pubsweet-component-hello-world.jsx` file into it
3. Update the `routes.jsx` file with the new path to our component:
```jsx
import Hello from './components/pubsweet-component-hello-world/pubsweet-component-hello-world.jsx'
```

### 2. Add a `package.json` file

In the `pubsweet-component-hello-world` folder, make a new file called `package.json`. This contains metadata so this can be treated as an npm package. We'll add the absolute minimum information:
```json
{
  "author": "John Smith",
  "main": "index.js",
  "name": "pubsweet-component-hello-world",
  "version": "0.1.0"
}
```
Add your own name in place of John's.

### 3. Add an `index.js` file

We've specified a main file - now we need to create it. We're making a PubSweet Client app, so we need to properly configure it. `index.js` should look like this:
```jsx
module.exports = {
  client: {
    components: [
      () => require('./pubsweet-component-hello-world')
    ]
  }
}
```
The package is complete! Now PubSweet will be able to tell that this is a client component (see [component documentation](/docs/core/client.html) for more information).

Next, let's lay the foundation for our PubSweet component to start [interacting with the rest of the app](./4_redux_actions.html).
