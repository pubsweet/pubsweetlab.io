# 6. Create a reducer
Now we need to define a reducer function, which will tell PubSweet what to do when our `'BUTTONCLICK'` action is dispatched. A Redux reducer is a pure function (deterministic, no side effects) that simply:
1. Takes in two arguments: 1) the current state, and 2) the dispatched action
2. Changes the state based on the dispatched action
3. Returns the new state

For more information on reducers, see [this documentation](http://redux.js.org/docs/basics/Reducers.html)

Let's add an additional key-value pair to the PubSweet state, that will track whether our button has been clicked before (`clickStatus: "clicked"`) or not (`clickStatus: "unclicked"`).

Create a new file in our component's home directory, called `reducers.js`:

```jsx
import * as T from './types'

export default function HelloReducer ( state = {
    clickStatus: 'unclicked'
  }, action ) {
  switch( action.type ) {
    case T.BUTTONCLICK:
      return Object.assign({}, state, {
        clickStatus: "clicked"
      })
    default:
      return state
  }
}
```

In the above reducer, note the following:
* We define an initial default state in the initial argument (`{ clickStatus: 'unclicked'}`). Since we are creating a frontend component, this information will be added to the application state.
* The function also takes in an `action` - this reducer is going to be run any time an action is dispatched.
* A switch statement checks the `action.type` and executes code when it recognizes an action type. If it doesn't recognize the `action.type`, it defaults to returning the original state with no changes. In this case, it changes `clickStatus` value to `"clicked"`.

Now we have a complete reducer that can recognize one action and update a state, but it's not hooked into anything. To use it, we need to register this reducer as a callback function on PubSweet's store. Then, each time an action is dispatched, the store will run all the reducers it has, including ours.

PubSweet command-line tools will do this for us when we [`add` our component to PubSweet](./7_add).
