# 2. Add component to route

We've made a basic component. Next, we'll add it to a route so it will render into our PubSweet app.

### 1. Open the  `routes.jsx` file, in the `[your_app_name]/app/` directory

This file imports pubsweet components, and then associates them with urls in the app.

### 2. `import` your component

Lines like these import PubSweet components so that the `routes.jsx` file can use them:

```jsx
import Login from 'pubsweet-component-login/Login'
import Signup from 'pubsweet-component-signup/Signup'
```

Near the other `import` lines, add one to import the new component:
```jsx
import Hello from './components/pubsweet-component-hello-world.jsx'
```
Our component doesn't come from the `node_modules` folder like the other imports in this file. For our component, we give a relative path to point to the component.

### 3. Add component to a new route

The router specifies what components to render for different urls:
```jsx
class Managed extends React.Component {
  render () {
    return <AuthenticatedManage>
      <Switch>
        <Route path='/manage/posts' component={PostsManager} />
        <Route path='/manage/users' component={UsersManager} />
        <Route path='/manage/teams' component={TeamsManager} />
        <Route path='/manage/sciencewriter/:id' component={MediumDraft} />
      </Switch>
    </AuthenticatedManage>
  }
}

export default (
  <Switch>
    <Route exact path='/' component={Blog} />
    <Route path='/manage' component={Managed} />
    <Route path='/login' component={Login} />
    <Route path='/signup' component={Signup} />
    <Route path='/password-reset' component={PasswordReset} />
    <Route path='/:id' component={HTML} />
  </Switch>
)
```
Let's add ours, and require users to login to see it. Put this under the `<Route path='/manage/sciencewriter/:id' component={MediumDraft} />` line:
```jsx
<Route path='hello' component={Hello} />
```
The router should now look like this:
```jsx
class Managed extends React.Component {
  render () {
    return <AuthenticatedManage>
      <Switch>
        <Route path='/manage/posts' component={PostsManager} />
        <Route path='/manage/users' component={UsersManager} />
        <Route path='/manage/teams' component={TeamsManager} />
        <Route path='/manage/sciencewriter/:id' component={MediumDraft} />
        <Route path='hello' component={Hello} />
      </Switch>
    </AuthenticatedManage>
  }
}

export default (
  <Switch>
    <Route exact path='/' component={Blog} />
    <Route path='/manage' component={Managed} />
    <Route path='/login' component={Login} />
    <Route path='/signup' component={Signup} />
    <Route path='/password-reset' component={PasswordReset} />
    <Route path='/:id' component={HTML} />
  </Switch>
)
```
Here's what we've done:
* `path='hello'` sets the url. Since it's nested under `/manage`, the url that will serve up this component is `/manage/hello`. And, since it's wrapped in the `AuthenticatedManage` tag, PubSweet requires users to sign in before accessing this route.
* `component={Hello}` specifies which component will be served from this endpoint.

### 4. See the component in the app

Try it out!

1. Start the PubSweet server if it's not already running (`pubsweet start` from the app's directory). You'll see `info: PubSweet is listening on port 3000` if it worked.
2. Go to `localhost:3000/manage/hello` in your browser. Since our route requires login, PubSweet should show you the login.
3. After you successfully log in, PubSweet should take you to `localhost:3000/manage/hello`.  You should see your new component doing its job and rendering "Hello world!" onto the page:
<img src="/images/tutorial/0_route.png" style="width: 100%;" />

### 5. Move the component

Now that we've proved to ourselves that we can load a component into a PubSweet app, let's move our component route outside the `AuthenticatedManage` component so we don't have to log in to see it (and reenter credentials every time we make a change):
```jsx
class Managed extends React.Component {
  render () {
    return <AuthenticatedManage>
      <Switch>
        <Route path='/manage/posts' component={PostsManager} />
        <Route path='/manage/users' component={UsersManager} />
        <Route path='/manage/teams' component={TeamsManager} />
        <Route path='/manage/sciencewriter/:id' component={MediumDraft} />
      </Switch>
    </AuthenticatedManage>
  }
}

export default (
  <Switch>
    <Route exact path='/' component={Blog} />
    <Route path='/manage' component={Managed} />
    <Route path='/login' component={Login} />
    <Route path='/signup' component={Signup} />
    <Route path='/password-reset' component={PasswordReset} />
    <Route path='/:id' component={HTML} />
    <Route path='hello' component={Hello} />
  </Switch>
)
```
This renders our component on its own page at "localhost:3000/hello". Try it out.

Next, let's [package it as a PubSweet component](./3_package.html).
