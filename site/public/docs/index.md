<!-- <img src="https:/gitlab.coko.foundation/pubsweet/pubsweet/raw/master/assets/rgb-medium.jpg" width="300" /> -->

# Pubsweet documentation <br/> Table of contents

## Core modules

* [Overview](/docs/core/overview.html)
* [Server](/docs/core/server.html)
* [Client](/docs/core/client.html)
* [CLI](/docs/core/cli.html)

## Components

* [Overview](/docs/components/index.html)
* [Using components](/docs/components/using.html)
* [Component library](/docs/components/library.html)
* [Developing components](/docs/components/developing.html)

## Community

* [Getting help](/docs/community/help.html)
* [Projects using PubSweet](/docs/community/projects.html)
* [Getting involved](/docs/community/contributing.html)
* [Contributors and credits](/docs/community/contributors.html)
